export const successResponse = (code, message, data) => {
  return {
    status: code,
    message: message,
    data: data,
  };
};
