// Libraries
const express = require("express");
const router = express.Router();

// Routes
const userRoute = require("./user");
const contactRoute = require("./contact");

module.exports = (app) => {
  router.use("/user", userRoute);
  router.use("/contact", contactRoute);

  app.use("/api", router);
};
