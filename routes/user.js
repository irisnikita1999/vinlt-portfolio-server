// Libraries
const router = require("express").Router();
const multer = require("multer");

// Controller
const {
  createUser,
  getListUser,
  getUserById,
  updateUser,
  loginUser,
  validateUser,
  searchUser,
  addFriend,
} = require("../controllers/user");

// Helpers
const { authenticateToken } = require("../helpers/auth");

// Multer middleware
let storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, "./uploads/avatar");
  },
  filename: function (req, file, cb) {
    cb(null, file.fieldname + "_" + Date.now() + "_" + file.originalname);
  },
});

let upload = multer({
  storage: storage,
}).single("avatar");

// Router
router.post("/", upload, createUser);
router.post("/login", loginUser);
router.post("/add-friend", addFriend);
router.post("/search", searchUser);
router.post("/validate", authenticateToken, validateUser);
router.get("/", authenticateToken, getListUser);
router.get("/:id", getUserById);
router.patch("/:id", upload, updateUser);

module.exports = router;
