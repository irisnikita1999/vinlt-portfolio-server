// Libraries
const jwt = require("jsonwebtoken");

/** Function generate access token
 *
 * @param {*} user
 * @returns
 */
exports.generateAccessToken = function (user) {
  return jwt.sign(user, process.env.TOKEN_SECRET, {
    expiresIn: process.env.EXPIRES_IN,
  });
};

/** Authenticate token
 *
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
exports.authenticateToken = function (req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader ? authHeader.split(" ")[1] : req.query.token;

  if (token == null) return res.sendStatus(401).json();

  jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
    console.log("🚀 ~ file: auth.js ~ line 29 ~ jwt.verify ~ err", err);
    if (err) return res.sendStatus(403).json();

    req.authEmail = user.email;

    next();
  });
};
