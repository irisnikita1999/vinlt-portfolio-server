const { sendMail } = require("../services/nodemailer");
const Contact = require("../models/contact");

// Template email
const { contactTemplate } = require("../templates/Email/contact");

const createContact = async (req, res) => {
  try {
    const { body } = req;

    const contact = new Contact(body);

    const contactCreated = await contact.save();

    sendMail({
      to: contact.email,
      subject: "Thanks for get in touch with me",
      html: contactTemplate({
        email: contact.email,
        name: contact.fullName,
      }),
    });

    res.json({
      statusCode: res.statusCode,
      data: contactCreated,
      message: "Create contact success",
    });
  } catch (error) {
    res
      .status(400)
      .json({ statusCode: res.statusCode, message: error.message });
  }
};

module.exports = {
  createContact,
};
