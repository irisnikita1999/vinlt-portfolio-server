// Libraries
const fs = require("fs"),
  bcrypt = require("bcrypt");

// Email template
const { verifyTemplate } = require("../templates/Email/verify");

// Helpers
const { generateAccessToken } = require("../helpers/auth");

// Constants
const { USER_FIELD } = require("../constants/user");

// Models
const User = require("../models/user");

// Constant
const {
  WRONG_PASS_MESS,
  USER_NOT_FOUND_MESS,
  STATUS_PENDING,
  USER_NOT_VERIFY_MESS,
  REGISTER_SUCCESS_MESS,
  EMAIL_EXITED_MESS,
} = require("../constants/user");

/** Create new user
 *
 * @param {*} res
 * @param {*} req
 */
const createUser = async (req, res) => {
  try {
    const user = req.body;
    const fileName = req.file ? req.file.filename : "";

    user.avatar = "avatar/" + fileName;

    const isExitedUser = (await User.findOne({ email: user.email }))
      ? true
      : false;

    if (isExitedUser) {
      throw { message: EMAIL_EXITED_MESS };
    }

    user.password = bcrypt.hashSync(user.password, 10);

    const newUser = await User.create(user);

    res.status(201).json({ message: REGISTER_SUCCESS_MESS });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

/** Login user
 *
 * @param {*} req
 * @param {*} res
 * @returns
 */
const loginUser = async (req, res) => {
  console.log("🚀 ~ file: user.js ~ line 76 ~ loginUser ~ req", req.body);
  try {
    const { email, password } = req.body;

    const user = await User.findOne({ email }).populate(
      "friends",
      "fullName email avatar"
    );

    if (user) {
      if (user.status === STATUS_PENDING) {
        throw { message: USER_NOT_VERIFY_MESS };
      }

      if (user.comparePassword(password)) {
        let token = generateAccessToken({ email: user.email });

        user.password = "";

        return res.status(200).json({
          token,
          user,
        });
      }

      throw { message: WRONG_PASS_MESS };
    }

    throw { message: USER_NOT_FOUND_MESS };
  } catch (error) {
    console.log(error);
    res.status(404).json({ message: error.message });
  }
};

/** Get list user
 *
 * @param {*} req
 * @param {*} res
 */
const getListUser = async (req, res) => {
  try {
    const users = await User.find({}, USER_FIELD);
    console.log("🚀 ~ file: user.js ~ line 113 ~ getListUser ~ users", users);

    res.status(200).json(users);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

/** Get user by id
 *
 * @param {*} req
 * @param {*} res
 */
const getUserById = async (req, res) => {
  try {
    const { id } = req.params;

    const user = await User.findById(id, USER_FIELD);

    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

/** Update user
 *
 * @param {*} req
 * @param {*} res
 */
const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    let new_avatar = "";

    if (req.file) {
      console.log("🚀 ~ file: user.js ~ line 154 ~ loginUser ~ error", error);
      new_avatar = req.file.filename;

      fs.unlinkSync("./uploads/" + req.body.old_avatar);
    } else {
      new_avatar = req.body.old_avatar;
    }

    const newUser = req.body;
    newUser.avatar = new_avatar;

    await User.findByIdAndUpdate(id, newUser);
    res.status(200).json({ message: "User updated successfully!" });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

const validateUser = async (req, res) => {
  try {
    const { authEmail } = req;

    const user = await User.findOne({ email: authEmail }, USER_FIELD).populate(
      "friends",
      "fullName email avatar"
    );

    if (!user) throw { message: USER_NOT_FOUND_MESS };

    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

const searchUser = async (req, res) => {
  try {
    const { search } = req.body;

    const user = await User.find({ $text: { $search: search } }, USER_FIELD);

    res.status(200).json(user);
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

const addFriend = async (req, res) => {
  try {
    const { userId, friendId } = req.body;

    const user = await User.findById(userId);
    const friendUser = await User.findById(friendId);

    user.friends.push(friendId);
    friendUser.friends.push(userId);

    await user.save();
    await friendUser.save();

    res.status(200).json({ message: "Add friend successfully" });
  } catch (error) {
    res.status(404).json({ message: error.message });
  }
};

module.exports = {
  createUser,
  getListUser,
  getUserById,
  updateUser,
  loginUser,
  validateUser,
  searchUser,
  addFriend,
};
